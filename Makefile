test_project_dir = /tmp/test-project

generate:
	scripts/generate_project.sh  $(test_project_dir)

lint: generate
	scripts/lint.sh $(test_project_dir)

build:
	exit 0

test: generate
	scripts/test.sh $(test_project_dir)
