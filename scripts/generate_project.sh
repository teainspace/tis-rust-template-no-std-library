#!/bin/bash
set -e

PROJECT_DIR=../$(basename $PWD)

rm -f -r $1

tis_project_generator $1 $PROJECT_DIR --template-pairs {{crate_name}} my-project
